package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"strconv"
)

func checkErr(err error) { // Helper function to log fatal errors and exit program.
	if err != nil {
		log.Fatal(err)
	}
}

func streamSort(filename string, topn int) ([]int, error) {
	var topValues []int = nil      // initalize empty slice
	file, err := os.Open(filename) // open file for reading
	checkErr(err)                  // throw fatal error if file cannot be opened
	defer file.Close()             // close file when function completes

	scanner := bufio.NewScanner(file) // prepare a new scanner to scan the file
	for scanner.Scan() {              // scan each line of the file breaking on error or EOF
		n, err := strconv.Atoi(scanner.Text()) // convert the strings to numbers
		checkErr(err)                          // catch error in the event strings cannot be coverted
		for i := 0; i < topn; i++ {            // start of binary sort
			if topValues == nil { // initilize the slice if its empty
				topValues = append([]int{n}, topValues...)
			}
			// Minor defect here. If the top N requested is larger then the amount of numbers in the file. The loop will duplicate the last number assigned to n instead of exiting.
			if n >= topValues[i] || len(topValues) < topn { // Start the comparison of the binary sort. If scanned number is higher then number at index position. Insert scanned number at current index position. Also automatically append when top values slice is smaller then reqeusted return.
				topValues = append(topValues, 0)
				copy(topValues[i+1:], topValues[i:])
				topValues[i] = n
				if len(topValues) > topn { // If slice size exceeds requested return trim the smallest numbers to keep sorting times low
					topValues = append(topValues[:len(topValues)-1], topValues[len(topValues):]...)
				}
				break // stop the search if the scanned number is smaller
			}

		}

	}
	return topValues, scanner.Err() // return the requested top values and errors
}

func main() {
	filenameptr := flag.String("f", "", "Location of the file to search.") // parameterize file location
	topnPtr := flag.Int("n", 1, "The amount of numbers to return.")        // parameterize n numbers

	flag.Parse() // parse flags for use

	topValues, err := streamSort(*filenameptr, *topnPtr) // execute streamSort function to return values
	checkErr(err)                                        // Catch errors from stream sort

	fmt.Printf("These are the top %d numbers: %d\n", *topnPtr, topValues) // print the request values to console
}
