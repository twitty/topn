# topN

Golang implementation of topN.

## Getting Started

### Generating seed file for testing
To generate a seed file for testing run the included `generate_seed_file.sh` script.
The default settings will generate a seed file with 50,000 random numbers up to 10,000,000 in size.

```sh
./generate_seed_file.sh
```

### Running topN

If you generated a seed file with the default settings you can execute the `run_topn.sh` script.

```sh
./run_topn.sh
```
It runs the following command:

```sh
go run main.go -f seedfile.txt -n 5
```


